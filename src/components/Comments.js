export default function Comments({ showComments, comments }) {
    return (
        <>
            {
                showComments && (
                    <>
                        {comments && comments.map((c) => (
                            <div key={c.id}>
                                <h2>{c.user}</h2>
                                <p>{c.comment}</p>
                            </div>
                        ))}
                    </>
                )
            }
        </>
    )
}

import video from "../data/video.js";
import Comments from "./Comments.js";
import { useState, useEffect } from "react";

function App() {
  const [dataVideo, setDataVideo] = useState(video);
  const [like, setLikes] = useState();
  const [dislike, setDisLikes] = useState();
  const [comments, setComments] = useState();
  const [commentsCant, setCommentsCant] = useState();
  const [showComments, setShowComments] = useState(true);
  const [action, setAction] = useState("hide comments");

  useEffect(() => {
    if (dataVideo) {
      console.log(dataVideo);
      setLikes(dataVideo.upvotes);
      setDisLikes(dataVideo.downvotes);
      setComments(dataVideo.comments);
      setCommentsCant(dataVideo.comments.length);
    }
  }, [dataVideo]);

  const hideComments = () => {
    setShowComments(!showComments);

    if(showComments && action != "hide comments"){
      setAction("hide comments");
    }else{
      setAction("Show Comments");
    }
  }

  const addLikes = () => {
    setLikes(like + 1);
  }

  const addDislikes = () => {
    setDisLikes(dislike + 1);
  }

  return (
    <div className="App">
      {
        dataVideo && (
          <div className="content">
            <iframe
              width="919"
              height="525"
              src={dataVideo.embedUrl}
              frameBorder="0"
              allowFullScreen
              title="Thinking in React"
            />
            <h1>{dataVideo.title}</h1>
            <p>{dataVideo.views} views | Uploaded {dataVideo.createdAt}</p>

            <div className="flexRow">
              <button onClick={addLikes}>
                {like}
              </button>
              <button onClick={addDislikes}>
                {dislike}
              </button>
            </div>
            <button className="mt-10" onClick={hideComments}>
              { action }
            </button>

            <div className="comments">
              <h1>{commentsCant} comments</h1>

              <Comments showComments={showComments} comments={comments} />
            </div>
          </div>
        )
      }
    </div>
  );
}

export default App;
